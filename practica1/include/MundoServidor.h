// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include <pthread.h>

#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"

#include "../include/Socket.h"

#define PUERTO 4444 //En las pruebas se debe cambiar el puerto del cliente y poner el que tiene el servidor

class CMundoServidor  
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();

	void print(char *mensaje, int x, int y, float r, float g, float b);	//añadido despues

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	
	char ruta_fifo_logger[256];	//ruta de la fifo de comunicación con el logger
	int fd_fifo_logger;		//descriptor de fichero de la fifo de comunicación con el logger

	Socket socket_conexion, socket_comunicacion;

	pthread_t hilo_escucha;

	void RecibeComandosJugador();
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
