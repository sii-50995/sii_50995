//Nombre: Alvaro Ruedas - 
// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "../include/MundoCliente.h"
#include "../include/glut.h"

#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#define RUTAMEMO "/tmp/mimemo"
#define MAXPUN 3
#define RUTAFIFO_CLIENTE_SERVIDOR "/tmp/mififo2"
#define RUTA_FIFO_SERVIDOR_CLIENTE "/tmp/mififo3"
#define TAM 256


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{	

	Init();

}

CMundoCliente::~CMundoCliente()
{
	this->socket_comunicacion.Close();
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void CMundoCliente::print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	char buffer[256], cad[256];
	int n;

	printf ("INICIO Ontimer con value=%d.\n", value);

	//El cliente recibe las coordenadas a través del socket en el OnTimer.
	
	n = this->socket_comunicacion.Receive(cad, 256);
	if (n <= 0){
		perror("Error leyebndo del socket de comunicación entre cliente y servidor.\n");
		exit(3);
	} else
		printf ("\tLectura correcta del socket.\n");
	//[]
	
	//Actualizar los atributos de MundoCliente con los datos recibidos (sscanf()).
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x, &esfera.centro.y, &jugador1.x1, &jugador1.y1, &jugador1.x2, 						&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);
	printf ("\tDatos actualizados.\n");
	

	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	}

	if (puntos1 == MAXPUN || puntos2 == MAXPUN){
		exit(0);	
	}

	printf ("\tMovimientos de raquetas y pelota realizados.\n");

	this->datos->esfera = this->esfera;
	this->datos->raqueta1 = this->jugador1;
	printf ("\tDatos actualizados guardados en memoria compartida mapeada.\n");
	
	//Invocar OnKeyboardDown
	switch(this->datos->accion){
		case 0://OnKeyboardDown('w',0,0);
		jugador1.velocidad.y=0;
		//printf("movimiento NADA\n");
		break;//jugador1.velocidad.y=-4;break;

		case -1:OnKeyboardDown('s',0,0);
		//printf("movimiento ABAJO\n");
		break;//jugador1.velocidad.y=-4;break;
		
		case 1:OnKeyboardDown('w',0,0);
		//printf("movimiento ARRIBA\n");
		break;//jugador1.velocidad.y=4;break;
		//case '0':OnKeyboardDown();break;//jugador1.velocidad.y=0;break;
		
		default:
		//printf("movimiento DESCONOCIDO\n");
		break;
	}
	printf ("\tAccion realizada.\n");

	//probar si  es necesario poner la velocidad a 0
	printf ("FIN Ontimer con value=%d.\n", value);

}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	char cad[256];
	int n;

	
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	}

	
	//Escribe los datos de la tubería (sprintf y write) en el OnKeyboardDown().
	//Construir la cadena de texto con los datos a enviar (sprintf())
	sprintf(cad,"%c", key);

	//El cliente envía la tecla a través del socket en el método OnKeyboardDown.
	n = this->socket_comunicacion.Send(cad, strlen(cad));
	if (n != strlen(cad))
		perror("Error escribiendo en el pipe de comunicación con el cliente.\n");
	

}

void CMundoCliente::Init()
{

	int resultado;
	char nombre[50];
	char ip[50];
	
	strcpy(ip, "127.0.0.1");	//IMPORTANTE!! aquí debe ir la ip del compañero que hace de servidor (ver ifconfig)

	printf("MundoCliente INICIO Init\n");

	//El cliente pide por teclado un nombre para enviárselo al servidor.
	printf("Introduce tu nombre: ");
	scanf("%s", nombre);

	
	//El cliente se conecta a la IP y puerto del servidor.
	if (this->socket_comunicacion.Connect(ip, PUERTO)<0)
		perror("Error de conexion con el socket");

	//Envía el nombre a través del socket.
	if (this->socket_comunicacion.Send(nombre, strlen(nombre)) <= 0) {
		perror("Error enviando el nombre al servidor.\n");
		exit(1);
	}

	

////////////////////

	//Fichero compartido con bot ------------------------------------------------------------

	this->datos = (DatosMemCompartida*) malloc(sizeof(DatosMemCompartida));
	if (this->datos == NULL)
		perror("Error malloc this->datos");
	else
		printf("Datos de memoria compartida reservados (malloc) correctamente.\n");

	//creacion del fichero de comunicación con el bot
	int fd_memo, tama_memo;

	fd_memo = open(RUTAMEMO, O_CREAT | O_TRUNC | O_RDWR, 0600);
	if(fd_memo<0){
		perror("Error creando el fichero de memoria compartida");
		exit(1);
	}
	else printf("Fichero de memoria compartida con bot creado.\n");

	
	tama_memo = write(fd_memo, (void*) datos, sizeof(DatosMemCompartida));
	
	if(tama_memo<sizeof(DatosMemCompartida)){
		perror("Error escribiendo el fichero de memoria compartida");
		exit(1);
	}
	//else printf("PASO 2: Escrito en el fichero de memeoria compartida.\n");

	
	

	this->datos = (DatosMemCompartida*) mmap(NULL, sizeof(DatosMemCompartida), PROT_WRITE | PROT_READ, MAP_SHARED, fd_memo, 0);	//Mirar sintaxis
	if (this->datos == MAP_FAILED){		
		perror("Error mapeando el fichero de memoria compartida");
		exit(1);
	}
	else printf("Fichero de memoria compartida con bot mapeado.\n");

	close(fd_memo);	

//////////////


	//printf("INICIO DEL METODO INIT\n");
	Plano p;
//pared inferior
	//printf("INICIO DEL METODO INIT 1\n");
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	//printf("INICIO DEL METODO INIT 2\n");
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	//printf("INICIO DEL METODO INIT 3\n");
	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	//printf("INICIO DEL METODO INIT 4\n");
	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	//printf("INICIO DEL METODO INIT 5\n");
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	//printf("INICIO DEL METODO INIT 6\n");
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//printf("INICIO DEL METODO INIT 7\n");

	printf("MundoCliente FIN Init\n");
	
}
