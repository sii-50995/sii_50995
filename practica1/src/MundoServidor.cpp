//Nombre: Alvaro Ruedas - 
// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "../include/MundoServidor.h"
#include "../include/glut.h"

#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#define RUTAFIFO_LOGGER "/tmp/mififo"
#define RUTAFIFO_CLIENTE_SERVIDOR "/tmp/mififo2"
#define RUTA_FIFO_SERVIDOR_CLIENTE "/tmp/mififo3"
#define MAXPUN 3



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoServidor::CMundoServidor()
{
	strcpy(this->ruta_fifo_logger, RUTAFIFO_LOGGER);
	Init();

}

CMundoServidor::~CMundoServidor()
{
	close(fd_fifo_logger);

	//Cerrar los sockets
	this->socket_conexion.Close();
	this->socket_comunicacion.Close();
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void CMundoServidor::print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	char buffer[256], cad[256];
	int n;


	//Construir la cadena de texto con los datos a enviar (sprintf())
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x, esfera.centro.y, jugador1.x1, jugador1.y1, jugador1.x2,
						jugador1.y2, jugador2.x1,jugador2.y1, jugador2.x2, jugador2.y2, puntos1, puntos2);


	//Enviar los datos por la tubería (write).
	n = this->socket_comunicacion.Send(cad, strlen(cad));
	if (n != strlen(cad))
		perror("Error escribiendo en el pipe de comunicación con el cliente.\n");

	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		sprintf(buffer,"Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",puntos2);
			
		if (write(fd_fifo_logger,buffer,strlen(buffer))<0)
			{printf("Error escribiendo 1");}
		
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		sprintf(buffer,"Jugador 1 marca 1 punto, lleva un total de %d puntos.\n",puntos1);
		
		if (write(fd_fifo_logger,buffer,strlen(buffer))<0)
			{printf("Error escribiendo 1\n");}
		//else
		//	{printf("Mensaje escrito 1");}
	}

	if (puntos1 == MAXPUN || puntos2 == MAXPUN){
		if (puntos1 == MAXPUN) {
			sprintf(buffer,"Fin del juego. Ha ganado la máquina (Jug. 1).\n");
			if (write(fd_fifo_logger,buffer,strlen(buffer))<0)
				{printf("Error escribiendo final\n");}
		}
		else {	
			sprintf(buffer,"Fin del juego. Ha ganado el humano (Jug. 2).\n");
			if (write(fd_fifo_logger,buffer,strlen(buffer))<0)
				{printf("Error escribiendo final\n");}}
		close(fd_fifo_logger);
		exit(0);	
	}

}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	}
}


void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d; //convierto el parámetro genéridco recibido en el tiupo de datos adeduado
      p->RecibeComandosJugador();
}

void CMundoServidor::RecibeComandosJugador(){

	while (1) {
            usleep(10);
            char cad[5];
            this->socket_comunicacion.Receive(cad, 5);
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }

}

void CMundoServidor::Init()
{

	char buffer[100];
	char ip[50];
	strcpy(ip, "127.0.0.1");

	//Enlazar el socket de conexión a una dirección IP y un puerto
	this->socket_conexion.InitServer(ip, PUERTO);
	//Socket de conexión se queda a la espera de una conexión escuchando en ese puerto.
	this->socket_comunicacion = this->socket_conexion.Accept(); //devuelve el socket que realizará la comunicación
	
	//El servidor recibe el nombre del cliente conectado a través del socket y lo imprime por pantalla
	if (this->socket_comunicacion.Receive(buffer, 100) <= 0) {
		perror("Error leyendo el nombre del cliente por el socket.\n");
		this->socket_comunicacion.Close();
		this->socket_conexion.Close();
		exit(1);
	} else {
		printf ("Nombre del cliente: '%s'\n", buffer);
	}
	


	//Crear el hilo de escucha
	if (pthread_create(&(this->hilo_escucha), NULL, hilo_comandos, this) != 0){
		printf ("Error creando el hilo.\n");
		exit(1);
	}	

	//pipe de comunicacion conb el logger
	this->fd_fifo_logger=open(this->ruta_fifo_logger, O_WRONLY);
	printf("%s\n",this->ruta_fifo_logger);
	if (this->fd_fifo_logger<0){
		perror("Error abriendo el pipe de comunicacion con el logger");
		exit(2);
	} else 
		printf ("FIFO servidor --> logger abierto.\n");
	

	//printf("INICIO DEL METODO INIT\n");
	Plano p;
//pared inferior
	//printf("INICIO DEL METODO INIT 1\n");
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	//printf("INICIO DEL METODO INIT 2\n");
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	//printf("INICIO DEL METODO INIT 3\n");
	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	//printf("INICIO DEL METODO INIT 4\n");
	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	//printf("INICIO DEL METODO INIT 5\n");
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	//printf("INICIO DEL METODO INIT 6\n");
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//printf("INICIO DEL METODO INIT 7\n");


	
}
