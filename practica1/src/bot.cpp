//Nombre: Alvaro Ruedas
//Meterle la n=read(arg) aparte y luego en los bucles simplemente la n
//Con el tema de los errores hay que comprober con el ultimo argumento (TAM_BUFFER,strlen()) si se ha leido/escrito el numero de bytes que realmente se pretendía que se escribiese
//return valor entre 1 y 255 para indicar un error, valor de finalización del proceso

#pragma once

#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>

//#include "../include/Raqueta.h"
#include "../include/DatosMemCompartida.h"
#include "../include/Esfera.h"

#define RUTAMEMO "/tmp/mimemo"


int main(int argc,char* argv[])
{
	int fd_memo, tama_memo;
      	DatosMemCompartida *datos;

	fd_memo = open(RUTAMEMO, O_RDWR);
	if(fd_memo<0){
		perror("Error abriendo el fichero de memoria compartida desde el bot");
		exit(1);
	}

	datos = (DatosMemCompartida*) mmap(NULL, sizeof(DatosMemCompartida), PROT_WRITE | PROT_READ, MAP_SHARED, fd_memo, 0);	//Mirar sintaxis
	
	close(fd_memo);

	if (datos == MAP_FAILED){		
		perror("Error mapeando el fichero de memoria compartida");
		exit(1);
	}
		
	while(1){
		usleep(25000);		//dificultad quitado 0
		
		printf("CentroP:%f Ry1:%f Ry2:%f", datos->esfera.centro.y, datos->raqueta1.y1, datos->raqueta1.y2);

		if(datos->esfera.centro.y > datos->raqueta1.y1-0.3){
			datos->accion = 1;
			//printf("BOT:Accion ARRIBA\n");
		}
		else if(datos->esfera.centro.y < datos->raqueta1.y2+0.3){
			datos->accion = -1;
			//printf("BOT:Accion ABAJO\n");
		}
		else{
			datos->accion = 0;
			//printf("BOT:Accion PARA\n");
		}
	}

	

	return 0;   
}

