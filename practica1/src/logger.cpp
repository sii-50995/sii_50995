//Nombre: Alvaro Ruedas
//Meterle la n=read(arg) aparte y luego en los bucles simplemente la n
//Con el tema de los errores hay que comprober con el ultimo argumento (TAM_BUFFER,strlen()) si se ha leido/escrito el numero de bytes que realmente se pretendía que se escribiese, no solo si ha dado o no un error
//MÁS: Terminar el bot, la práctica 4 y hacer la ampliación de ejercicios propuesta
//Re-estudiar todo con el fin de hacer un laboratorio perfecto.
//return valor entre 1 y 255 para indicar un error, valor de finalización del proceso
//Imprimir las practicas y encuadernarlas para estudiarlas bien


#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#define RUTAFIFO "/tmp/mififo"
#define TAM 256

int main(int argc,char* argv[])
{
	int resultado, fd, n;
	char buffer[TAM];
		resultado = mkfifo(RUTAFIFO, 0600);
		if (resultado==-1){
			perror("Error creando el fifo.");
			return 1;
		}
		fd = open(RUTAFIFO, O_RDONLY);
		if (fd==-1){
			perror("Error abriendo el fifo.");
			return 2;
		}

	//Hueco para el logger

	n = read(fd,buffer,TAM);
	int nbytes = 0;

	while(n>0){
		//fprintf(1,"%s",buffer);
		//printf("Mensaje leído");
		nbytes = write(1, buffer, n);
		if (nbytes < 0) {
			perror("Error escribiendo");
			close(fd);			
			return 3;
		}
		else if (nbytes != n){
			perror("No se escribieron todos los datos");
			close(fd);			
			return 4;		
		}
		//printf("Mensaje leído");
		n = read(fd,buffer,TAM);

	}


	close(fd);
	
	unlink(RUTAFIFO);

	return 0;   
}

